package nl.wur.ssb;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.rdfhdt.hdt.hdt.HDT;

public class SPARQLInterface {

    /**
     * ENABLES HDT QUERYING AND ENDPOINT QUERYING
     */

    /**
     *
     * @param hdt
     * @param queryFile
     * @param args
     * @return
     * @throws Exception
     */
    public static Iterable<ResultLine> query(HDT hdt, String queryFile, Object... args) throws Exception {
        if (hdt != null) {
            // QUERY THE HDT FILE
            Iterable<ResultLine> results = MyUI.rdfSimpleCon.runQuery(hdt, queryFile, args);
            hdt.close();
            return results;
        } else {
            return MyUI.rdfSimpleCon.runQuery(queryFile, true, args);
        }
    }

    public static Iterable<ResultLine> query(String queryFile, Object... args) throws Exception {
        return SPARQLInterface.query(null, queryFile, args);
    }

}
