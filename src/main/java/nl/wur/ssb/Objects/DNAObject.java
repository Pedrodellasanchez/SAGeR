package nl.wur.ssb.Objects;

public class DNAObject implements java.io.Serializable {

    public String type;
    private String sample;
    private String sequence;
    private String URI;
    private String accession;
    private int sequenceVersion;
    private int length;
    private String description;
    private String genes;

    public DNAObject() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public String getAccession() {
        return accession;
    }

    public void setAccession(String accession) {
        this.accession = accession;
    }

    public String getGenes() {
        return genes;
    }

    public void setGenes(String genes) {
        this.genes = genes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSequenceVersion() {
        return sequenceVersion;
    }

    public void setSequenceVersion(int sequenceVersion) {
        this.sequenceVersion = sequenceVersion;
    }
}
