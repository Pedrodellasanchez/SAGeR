package nl.wur.ssb.Objects;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class SPARQLResults {

    private Map<String, String> properties = new HashMap<String, String>();
    private Map<String, Callable<Object>> callables = new HashMap<String, Callable<Object>>();

    public String getProperty(String key) {
        return properties.get(key);
    }

    public void setProperty(String key, String value) {
        properties.put(key, value);
    }

    public Object call(String key) throws Exception {
        Callable<Object> callable = callables.get(key);
        if (callable != null) {
            return callable.call();
        }
        return null;
    }

    public void define(String key, Callable<Object> callable) {
        callables.put(key, callable);
    }
}
