package nl.wur.ssb;


import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import nl.wur.ssb.Objects.DNAObject;
import nl.wur.ssb.Objects.FeatureObject;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.vaadin.gridutil.cell.GridCellFilter;

public class GenomeBrowser extends VerticalLayout implements View {

    private HorizontalLayout topLayout = new HorizontalLayout();
    private HorizontalLayout downLayout = new HorizontalLayout();
    private Panel topLeftPanel = new Panel();
    private VerticalLayout topLeftSide = new VerticalLayout();
    private VerticalLayout topRightSide = new VerticalLayout();
    private TabSheet genomeTabs = new TabSheet();
    private VerticalLayout geneticTab = new VerticalLayout();
    private VerticalLayout externalTab = new VerticalLayout();
    private VerticalLayout pubTab = new VerticalLayout();
    private VerticalLayout featureTab = new VerticalLayout();
    //    private Button backButton;
    //    private RDFSimpleCon rdfSimpleCon = new RDFSimpleCon("");
    private String genomeName;


    // Throws Exception because of rdfSimpleCon...
    public GenomeBrowser() throws Exception {

        setMargin(false);
        setSpacing(false);
        setSizeFull();
        setHeight("100%");
        topLayout.setHeight("100%");
        topLayout.setWidth("100%");
        topLayout.setSpacing(false);
        downLayout.setHeight("100%");
        downLayout.setWidth("100%");
        topLayout.setMargin(false);
        downLayout.setMargin(false);
        topLeftSide.setMargin(false);
        topLeftSide.setSpacing(false);
        topRightSide.setMargin(false);
        //    backButton = new Button("Back", click -> Page.getCurrent().setUriFragment("!genomeView"));
        //        backButton = new Button("Back", click -> MyUI.navigator.navigateTo(MyUI.genomeView));
        //        backButton.setStyleName("backButton");
        topLeftPanel.setHeight("330px");
        //        topLeftSide.addComponents(backButton, topLeftPanel);
        topLayout.addComponents(topLeftSide, topRightSide);
        topLayout.setExpandRatio(topLeftSide, 80);
        topLayout.setExpandRatio(topRightSide, 20);
        addComponents(topLayout, downLayout);
        setExpandRatio(topLayout, 50);
        setExpandRatio(downLayout, 50);

        genomeTabs.setStyleName("tabSheet");
        geneticTab.setStyleName("geneticTab");
        genomeTabs.addTab(geneticTab, "Genetic info");
        genomeTabs.addTab(externalTab, "External Links");
        genomeTabs.addTab(pubTab, "Publications");
        genomeTabs.addTab(featureTab, "Features");
        featureTab.setVisible(false);
        setMargin(true);
        setStyleName("content");

        // When working with HDT files

        // TODO improve HDT handling...
        MyUI.logger.info("Current URI: " + Page.getCurrent().getUriFragment());
        // Breaks string in 2 pieces of which the last is the HDT file or HTTP address
        String[] path = Page.getCurrent().getUriFragment().split("/", 2);
        genomeName = path[path.length - 1];

        if (genomeName.matches(".*\\.dat")) {
            makeGenomeTable();
        } else if (genomeName.contains("http")) {
            makeGenomeTable();
        }

        // Remove progress bar when done?
    }


    public static Label generateLabelOutput(Map<String, String> labelInput) {
        StringBuilder content = new StringBuilder();
        content.append("<p><table>");
        for (String label : labelInput.keySet()) {
            content.append("<tr><td>").append(WordUtils.capitalize(label)).append("</td><td>").append(labelInput.get(label)).append("</td></tr>");
        }
        content.append("</p></table>");
        //    System.out.println(content.toString()+"\n");
        Label output = new Label(content.toString(), ContentMode.HTML);
        output.setStyleName("samplelabel");
        return output;
    }

    private void makeGenomeTable() {
        /*
         * this section fills the tab sheet with "contigs"...
         */


        try {
            File hdtFile = new File(MyUI.genomePath + "/" + genomeName);

            // In case of endpoint being null
            HDT hdt = null;
            if (hdtFile.exists()) {
                // Connecting to the HDT file
                hdt = HDTManager.mapIndexedHDT(hdtFile.getAbsolutePath(), null);
            }

            // Generates top part of page
            MyUI.logger.info("Generating top part of page");
            generateTopPart(hdt);

            /*
             * Filling the sequence tab with DNAObjects
             */

            // TODO this might go wrong for HDT files
            MyUI.logger.info("Generating Xref Labels");
            generateXrefLabel(hdt, "getSampleXref.sparql", genomeName); // .split("\\.")[0]

            MyUI.logger.info("Generating DNAObject table");
            createDNAObjectTable(hdt, genomeName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createDNAObjectTable(HDT hdt, String sample) throws Exception {
        List<DNAObject> dnaObjects = new ArrayList<>();
        int counter = 0;
        for (ResultLine resultLine : SPARQLInterface.query(hdt, "getDNAObjects.sparql", sample)) {
            counter = counter + 1;
            if (counter % 1000 == 0)
                MyUI.logger.info("Counter: " + counter);
            DNAObject dnaObject = new DNAObject();
            //?sample ?dnaobject ?type ?accession ?length ?strandType ?topology
            dnaObject.setURI(resultLine.getIRI("dnaobject"));
            dnaObject.setType(resultLine.getIRI("type").replaceAll(".*/", ""));
            dnaObject.setAccession(resultLine.getLitString("accession"));
            try {
                dnaObject.setLength(resultLine.getLitInt("length"));
            } catch (Exception e) {
                // lit == null issue for integer...
            }

            dnaObjects.add(dnaObject);
        }

        Grid<DNAObject> dnaObjectGrid = new Grid<>();
        dnaObjectGrid.setSizeUndefined();
        dnaObjectGrid.setWidth("100%");
        dnaObjectGrid.setHeight("300px");
        dnaObjectGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        dnaObjectGrid.setItems(dnaObjects);
        // TODO set dynamically?... otherwise streamline the query to make it even faster....
        // See SPARQLView example with dynamic object creation
        GridCellFilter<DNAObject> filter = new GridCellFilter<>(dnaObjectGrid, DNAObject.class);
        dnaObjectGrid.addColumn(DNAObject::getAccession).setCaption("Accession").setId("accession");
        dnaObjectGrid.addColumn(DNAObject::getType).setCaption("Type").setId("type");
        dnaObjectGrid.addColumn(DNAObject::getLength).setCaption("Length").setId("length");
        //    dnaObjectGrid.addColumn(DNAObject::getSequenceVersion).setCaption("Sequence Version");
        filter.setTextFilter("accession", true, true, "Accession...");
        filter.setTextFilter("type", true, true, "Type...");
        filter.setTextFilter("length", true, true, "Length...");
        dnaObjectGrid.setStyleName("grid");

        /*
         * Creating a listener to fill the feature tab
         */

        dnaObjectGrid.addSelectionListener(event -> {
            Set<DNAObject> selected = event.getAllSelectedItems();

            generateFeatureTable(hdt, selected.iterator().next().getURI());
            generateXrefLabel(hdt, "getSampleXref.sparql", genomeName.split("\\.")[0]);
            //Setup for features tab TODO erase old content, grid and tabs in right place!? Tabchangelistener....
        });

        geneticTab.addComponent(dnaObjectGrid);
        downLayout.addComponents(genomeTabs);
    }

    /*
     * Creating the feature tab
     */
    private void generateFeatureTable(HDT hdt, String selected) {
        boolean results = false;
        List<FeatureObject> featureObjects = new ArrayList<>();
        try {
            featureTab.removeAllComponents();
            String prevFeature = "";
            MyUI.logger.info("SELECTED: " + selected);
            for (ResultLine feature : SPARQLInterface.query(hdt, "getFeatures.sparql", selected)) {
                results = true;
                FeatureObject featureObject = new FeatureObject();
                featureObject.setURI(feature.getIRI("feature"));
                featureObject.setType(feature.getIRI("type").replaceAll(".*/", ""));
                featureObject.setLocusTag(feature.getLitString("locus"));
                featureObject.setBegin(feature.getLitInt("beginPos"));
                featureObject.setEnd(feature.getLitInt("endPos"));
                featureObject.setStrand(feature.getIRI("strand").replaceAll(".*/", ""));
                featureObject.setProduct(feature.getLitString("product"));
                featureObjects.add(featureObject);
            }

            if (results) {
                Grid<FeatureObject> featureGrid = new Grid<>();

                featureGrid.setWidth("100%");
                featureGrid.setHeight("300px");
                featureGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
                featureGrid.setItems(featureObjects);
                featureGrid.addColumn(FeatureObject::getLocusTag).setCaption("LocusTag").setId("locusTag");
                featureGrid.addColumn(FeatureObject::getProduct).setCaption("Product").setId("product");
                featureGrid.addColumn(FeatureObject::getType).setCaption("Type").setId("type");
                featureGrid.addColumn(FeatureObject::getBegin).setCaption("Begin").setId("begin");

                featureGrid.addColumn(FeatureObject::getEnd).setCaption("End").setId("end");

                featureGrid.setStyleName("grid");
                GridCellFilter<FeatureObject> filter = new GridCellFilter<>(featureGrid, FeatureObject.class);
                filter.setTextFilter("locusTag", true, false, "LocusTag...");
                filter.setTextFilter("product", true, false, "Product...");
                filter.setTextFilter("type", true, false, "Type...");
                filter.setTextFilter("begin", true, true, "Begin...");
                filter.setTextFilter("end", true, true, "End...");
                featureTab.addComponent(featureGrid);
                // Switch to feature tab
                genomeTabs.setSelectedTab(featureTab);
                featureTab.setVisible(true);

                featureGrid.addItemClickListener(itemClick -> generateGeneLabel(hdt, itemClick.getItem().getURI()));
            } else {
                Notification.show("No features for this dnaobject", Type.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void generateGeneLabel(HDT hdt, String geneIRI) {
        String queryGeneFile = "getGene.sparql";
        String queryTranscriptFile = "getTranscript.sparql";

        topLeftSide.removeAllComponents();
        topRightSide.removeAllComponents();
        // topLeftSide.addComponent(backButton);
        String transcriptSequence = "";
        String proteinSequence = "";
        String strand;
        Map<String, String> geneInfo = new LinkedHashMap<>();
        try {
            MyUI.logger.info("Get transcript info for: " + geneIRI);
            for (ResultLine gene : SPARQLInterface.query(hdt, queryTranscriptFile, geneIRI)) {
                topLeftPanel.setCaption("Gene overview");
                geneInfo.put("<h3>Overview</h3>", "<br>");

                // Multiple locus tags possible
                geneInfo = geneInfoPutter(geneInfo, "Locus Tag", gene.getLitString("locusTag"));

                if (gene.getIRI("strand").equals(" http://gbol.life/0.1/ForwardStrandPosition")) {
                    strand = " (+ strand)";
                } else {
                    strand = " (- strand)";
                }

                // Only one genomic location possible
                geneInfo = geneInfoPutter(geneInfo, "Genomic location", gene.getLitString("beginPos") + " - " + gene.getLitString("endPos") + strand);

                // Only one transcript type possible
                geneInfo = geneInfoPutter(geneInfo, "Transcript type", gene.getIRI("transcriptType").replaceAll(".*/", ""));

                transcriptSequence = gene.getLitString("transcriptSequence");
            }

            // TODO check for multiple results
            MyUI.logger.info("Get protein info for: " + geneIRI);
            for (ResultLine gene : SPARQLInterface.query(hdt, queryGeneFile, geneIRI)) {

                geneInfo = geneInfoPutter(geneInfo, "<h3>Product overview</h3>", "<br>");

                if (gene.getLitString("product") != null) {
                    geneInfo = geneInfoPutter(geneInfo, "Product", gene.getLitString("product"));
                }
                if (gene.getLitString("proteinId") != null) {
                    geneInfo = geneInfoPutter(geneInfo, "Protein ID", gene.getLitString("proteinId"));
                }
                geneInfo = geneInfoPutter(geneInfo, "Signature Description", gene.getLitString("signatureDesc"));
                geneInfo = geneInfoPutter(geneInfo, "<h3>Cross references</h3>", "<br>");
                geneInfo = geneInfoPutter(geneInfo, gene.getLitString("id"), ("<a href=" + gene.getLitString("proteinAddress") + " target=_blank>" + gene.getLitString("proteinAccession") + "</a>"));
                geneInfo = geneInfoPutter(geneInfo, gene.getLitString("secondaryDBId"),
                    ("<a href=" + gene.getLitString("secondaryProteinAddress") + " target=_blank>" + gene.getLitString("secondaryAccession") + "</a>"));
                transcriptSequence = gene.getLitString("transcriptSequence");
                proteinSequence = gene.getLitString("proteinSequence");
            }
        } catch (Exception e) {
            MyUI.logger.error(e);
            e.printStackTrace();
        }

        // Adding buttons and popup for sequence retrieval

        MyUI.logger.info("Generating label output");
        Label gene = generateLabelOutput(geneInfo);
        MyUI.logger.info("LABEL GENE: " + gene.getValue());
        topLeftPanel.setContent(gene);
        topLeftSide.addComponent(topLeftPanel);

        VerticalLayout sequencePopup = new VerticalLayout();
        TextArea sequenceTextArea = new TextArea();

        sequenceTextArea.setWidth("300px");
        sequenceTextArea.setHeight("300px");

        // Final needed for textArea
        final String transcriptSequenceText = transcriptSequence;
        final String proteinSequenceText = proteinSequence;

        PopupView popup = new PopupView(null, sequencePopup);

        Button getgeneSequence = new Button("Gene transcript sequence", click -> {
            sequenceTextArea.setValue(transcriptSequenceText);
            popup.setPopupVisible(true);
            setToClipboard(transcriptSequenceText);
            new Notification(null, "sequence copied to clipboard", Type.WARNING_MESSAGE, true).show(Page.getCurrent());
        });
        Button getproteinSequence = new Button("Protein sequence", click -> {
            sequenceTextArea.setValue(proteinSequenceText);
            popup.setPopupVisible(true);
            setToClipboard(proteinSequenceText);
            new Notification(null, "sequence copied to clipboard", Type.WARNING_MESSAGE, true).show(Page.getCurrent());
        });
        Button blastGene = new Button("BLAST gene", click -> {
            Notification.show("Not functional yet...", Type.ERROR_MESSAGE);
            //      Page.getCurrent().setUriFragment("!"+MyUI.blastView+"/" + transcriptSequenceText);
        });
        Button blastProtein = new Button("BLAST protein", click -> {
            Page.getCurrent().setUriFragment("!" + MyUI.blastView + "/" + proteinSequenceText);
        });

        sequencePopup.addComponents(sequenceTextArea);
        topRightSide.addComponents(getgeneSequence, getproteinSequence, popup, blastGene, blastProtein);

    }

    private Map<String, String> geneInfoPutter(Map<String, String> geneInfo, String key, String value) {
        if (value == null) {
            return geneInfo;
        }

        // Entry already exists append if not create new
        if (geneInfo.get(key) != null && !geneInfo.get(key).contains(value)) {
            geneInfo.put(key, geneInfo.get(key) + "; " + value);
        } else {
            geneInfo.put(key, value);
        }

        return geneInfo;
    }

    private void setToClipboard(String sequence) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection stringSelection = new StringSelection(sequence);
        ;
        clipboard.setContents(stringSelection, null);
    }

    //TODO make dynamic, able to change sparql file
    private void generateXrefLabel(HDT hdt, String queryFile, String sample) {

        externalTab.removeAllComponents();

        Map<String, String> xrefs = new LinkedHashMap<>();
        try {
            for (ResultLine xref : SPARQLInterface.query(hdt, queryFile, sample)) {
                xrefs.put(xref.getLitString("id"), ("<a href=" + xref.getLitString("address") + ">" + xref.getLitString("accession") + "</a>"));
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
        Label xref = generateLabelOutput(xrefs);
        externalTab.addComponent(xref);
    }

    private void generateTopPart(HDT hdt) throws Exception {
        // Get Sample information for top part page
        Map<String, String> geneOverview = new LinkedHashMap<>();

        // Additional contig information
        Iterator<ResultLine> numberOfContigs = SPARQLInterface.query(hdt, "getNumberOfContigsWithFeatures.sparql").iterator();
        if (numberOfContigs.hasNext()) {
            geneOverview.put("#Contigs with features", Integer.toString(numberOfContigs.next().getLitInt("contigCount")));
        }

        // Additional contig information
        numberOfContigs = SPARQLInterface.query(hdt, "getNumberOfContigs.sparql").iterator();
        if (numberOfContigs.hasNext()) {
            geneOverview.put("#Contigs", Integer.toString(numberOfContigs.next().getLitInt("contigCount")));
        }

        Iterator<ResultLine> results = SPARQLInterface.query(hdt, "getSampleInformation.sparql").iterator();

        // Creates Genome
        Genome object = Genome.class.cast(createObject(Genome.class, results));
        // generate generic overview by using all the getters of the genome object
        topLeftPanel.setCaption("Genome overview");
        //    geneOverview.put("<h3>Genome overview</h3>","<br>");

        // For each get function in the genome class
        for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(Genome.class).getPropertyDescriptors()) {
            // Getting the method call name (e.g. getLabHost)
            String method = propertyDescriptor.getReadMethod().getName();

            try {
                // Everything excepts the root (getURI)
                if (!method.matches("getURI")) {
                    // Try to call it
                    Method call = Genome.class.getDeclaredMethod(method);
                    // Try to get the results using the genome object
                    String result = (String) call.invoke(object);

                    if (result != null && !result.startsWith("http")) {
                        method = WordUtils.capitalize(method.replaceAll("^get", "").replaceAll("(.)([A-Z])", "$1 $2").toLowerCase());
                        geneOverview.put(method, result);
                    }
                }
            } catch (NoSuchMethodException e) {
                // To trace what could be added in the future
                // TODO we need to find a way to create getters and setters dynamically
                System.err.println("Getting: This property is ignored: " + method);
            }
        }

        Label genomeOverview = (generateLabelOutput(geneOverview));
        topLeftPanel.setContent(genomeOverview);
        topLeftSide.addComponent(topLeftPanel);
        //    topLeftSide.addComponent(genomeOverview);
    }

    /**
     * @return class object filled with setPossibilities, only accepts ?subject ?predicate ?object
     * ?typeObject
     */
    private Object createObject(Class clazz, Iterator<ResultLine> resultLines) {
        try {
            Object object = clazz.newInstance();
            Map<String, Method> settersMap = isSetter(clazz);
            String typeObject;
            String predicate;
            while (resultLines.hasNext()) {
                ResultLine resultLine = resultLines.next();
                typeObject = resultLine.getIRI("typeObject");
                predicate = WordUtils.capitalize(resultLine.getIRI("predicate").replaceFirst(".+0\\.1/", ""));
                // Subject should be added as the IRI of the object if available -- != NULL added as I do not understand the code yet
                if (resultLine.getIRI("subject") != null) {
                    settersMap.get("URI").invoke(object, resultLine.getIRI("subject"));
                }
                try {
                    if (typeObject == null) {
                        settersMap.get(predicate).invoke(object, resultLine.getIRI("object"));
                    } else if (typeObject.endsWith("string")) {
                        settersMap.get(predicate).invoke(object, resultLine.getLitString("object"));
                    } else if (typeObject.endsWith("integer")) {
                        settersMap.get(predicate).invoke(object, resultLine.getLitInt("object"));
                    }
                } catch (IllegalArgumentException | NullPointerException ignored) {
                }
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Gets setters from object
    private Map<String, Method> isSetter(Class aClass) {

        Map<String, Method> settersMap = new HashMap<>();
        Method methods[] = aClass.getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                if (method.getParameterTypes().length != 0) {
                    settersMap.put(StringUtils.substringBetween(method.toString(), ".set", "("), method);
                }
            }
        }
        return settersMap;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}



