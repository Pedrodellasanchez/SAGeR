package nl.wur.ssb;


import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;
import java.util.Set;

import javafx.scene.control.ProgressIndicator;
import nl.wur.ssb.Objects.Genome;
import org.vaadin.gridutil.cell.GridCellFilter;

public class GenomeOverview extends VerticalLayout implements View {

    public GenomeOverview() {

        System.out.println("GENOME OVERVIEW DEBUGGING...");
        Grid<Genome> grid = new Grid<>();

        grid.setWidth("100%");
        Label label = new Label("Genomic overview");
        addComponents(label);
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        grid.addSelectionListener(event -> {
            // Moving to a "new" page with genome information "allSelectedItems" is only one as grid by
            // default allows only one click
            Set<Genome> selected = event.getAllSelectedItems();
            Page.getCurrent().setUriFragment("!genomeBrowser/" + selected.iterator().next().getID());
        });

        grid.setItems(MyUI.genomes);
        grid.addColumn(Genome::getScientificName).setCaption("Scientific name").setId("name");
        grid.addColumn(Genome::getStrain).setCaption("Strain").setId("strain");
        grid.addColumn(Genome::getSpecies).setCaption("Species").setId("species");
        grid.addColumn(Genome::getGenus).setCaption("Genus").setId("genus");
        grid.addColumn(Genome::getFamily).setCaption("Family").setId("family");
        grid.addColumn(Genome::getOrder).setCaption("Order").setId("order");
        grid.addColumn(Genome::getClazz).setCaption("Class").setId("class");
        grid.addColumn(Genome::getPhylum).setCaption("Phylum").setId("phylum");
        grid.addColumn(Genome::getSuperkingdom).setCaption("Superkingdom").setId("superkingdom");
        grid.getColumns().forEach(column -> column.setHidable(true));
        GridCellFilter<Genome> filter = new GridCellFilter<>(grid, Genome.class);
        filter.setTextFilter("name", true, false, "Scientific Name...");
        filter.setTextFilter("strain", true, false, "Strain...");
        filter.setTextFilter("species", true, false, "Species...");
        filter.setTextFilter("genus", true, false, "Genus...");
        filter.setTextFilter("family", true, false, "Family...");
        filter.setTextFilter("order", true, false, "Order...");
        filter.setTextFilter("class", true, false, "Class...");
        filter.setTextFilter("phylum", true, false, "Phylum...");
        filter.setTextFilter("superkingdom", true, false, "Superkingdom...");
        addComponents(grid);


    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Something upon entering
    }
}
