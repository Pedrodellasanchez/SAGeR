package nl.wur.ssb.Objects;

import com.vaadin.ui.Grid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.jena.query.QuerySolution;
import org.rdfhdt.hdt.hdt.HDT;

public class GenericFunctions {

    /**
     * @param results a list of Jena ResultSets when querying multiple files
     * @return a grid matrix for Vaadin
     */
    public static Grid<HashMap<String, String>> DynamicGridJena(List<QuerySolution> results, List<String> resultVars) {
        // Content
        List<HashMap<String, String>> rows = new ArrayList<>();
        // Header
        for (QuerySolution row : results) {
            HashMap<String, String> fakeBean = new HashMap<>();
            for (String var : resultVars) {
                if (row.get(var) != null)
                    fakeBean.put(var, row.get(var).toString());
                else
                    fakeBean.put(var,"");
            }
            rows.add(fakeBean);
        }

        // Create the grid and set its items
        Grid<HashMap<String, String>> grid = new Grid<>();
        grid.setItems(rows);

        // Add the columns based on the first row
        HashMap<String, String> s = rows.get(0);
        for (Map.Entry<String, String> entry : s.entrySet()) {
            grid.addColumn(h -> h.get(entry.getKey())).setCaption(StringUtils.capitalize(entry.getKey()));

        }
        return grid;
    }

    /**
     * Only listens to ?subject ?predicate ?object and ?typeObject, will set ?subject as URI
     */
    public static Grid<HashMap<String, String>> DynamicGrid(HDT hdt, String queryFile, RDFSimpleCon rdfSimpleCon) {
        List<HashMap<String, String>> rows = new ArrayList<>();
        // SELECT DISTINCT ?subject ?predicate ?object ?typeObject
        try {
            Iterator<ResultLine> results = rdfSimpleCon.runQuery(hdt, queryFile).iterator();
            String subject = "";
            HashMap<String, String> fakeBean = null;
            while (results.hasNext()) {
                ResultLine result = results.next();
                if (subject.length() == 0) {
                    subject = result.getIRI("subject");
                    fakeBean = new HashMap<>();
                } else if (!subject.matches(result.getIRI("subject"))) {
                    rows.add(fakeBean);
                    fakeBean = new HashMap<>();
                }
                String predicate = WordUtils.capitalize(result.getIRI("predicate").replaceAll(".*/", ""));
                if (result.getIRI("typeObject") == null) {
                    //          System.out.println(result);
                } else {
                    fakeBean.put(predicate, result.getLitString("object"));
                }
            }
            // Create the grid and set its items
            Grid<HashMap<String, String>> grid = new Grid<>();
            grid.setItems(rows);

            // Add the columns based on the first row
            HashMap<String, String> s = rows.get(0);
            for (Map.Entry<String, String> entry : s.entrySet()) {
                grid.addColumn(h -> h.get(entry.getKey())).setCaption(entry.getKey());
            }
            return grid;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
