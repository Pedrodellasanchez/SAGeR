package nl.wur.ssb;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Iterator;
import junit.framework.TestCase;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang.WordUtils;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;

public class testEnvironment extends TestCase {

    public void testGenome() throws Exception {
        File genomeFile = new File("/Users/jasperkoehorst/GitLab/WebsiteDev/SAGeR/SAGeR/Genomes/GCA_000006685.dat");
        Domain domain = new Domain("");
        HDT hdt = HDTManager.mapIndexedHDT(genomeFile.getAbsolutePath(), null);
        Iterator<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, "getSampleInformation.sparql").iterator();
        Genome genome = new Genome();
        while (resultLines.hasNext()) {
            ResultLine resultLine = resultLines.next();

            String method = "set" + WordUtils.capitalize(resultLine.getIRI("predicate").replaceAll(".*/", ""));

            try {
                Method call = Genome.class.getDeclaredMethod(method, String.class);
                call.invoke(genome, resultLine.getLitString("object"));
            } catch (NoSuchMethodException e) {
                System.err.println("This property is ignored: " + method);
            }

            System.err.println(genome.getPlasmid());
        }

        for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(Genome.class).getPropertyDescriptors()) {

            // propertyEditor.getReadMethod() exposes the getter
            // btw, this may be null if you have a write-only property
            System.out.println(propertyDescriptor.getReadMethod());
            String method = propertyDescriptor.getReadMethod().getName();
            try {
                Method call = Genome.class.getDeclaredMethod(method);
                System.err.println(call.invoke(genome));
            } catch (NoSuchMethodException e) {
                System.err.println("This property is ignored 2: " + method);
            }
        }
    }
}
