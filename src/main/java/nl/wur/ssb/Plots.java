package nl.wur.ssb;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.LineChartConfig;
import com.byteowls.vaadin.chartjs.data.Dataset;
import com.byteowls.vaadin.chartjs.data.LineDataset;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.CategoryScale;
import com.byteowls.vaadin.chartjs.options.scale.LinearScale;
import com.byteowls.vaadin.chartjs.utils.ColorUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

public class Plots {

    public static ChartJs pancore(File table) throws FileNotFoundException {
        LineChartConfig lineConfig = new LineChartConfig();
        String color;
        // add data
        Scanner tableScanner = new Scanner(table);
        String[] header = tableScanner.nextLine().split("\t");

        lineConfig.data().labels(header).addDataset(new LineDataset().label("Core Min").fill(false)).addDataset(new LineDataset().label("Core Mean").fill(false))
            .addDataset(new LineDataset().label("Core Max").fill(false)).addDataset(new LineDataset().label("Pan Min").fill(false)).addDataset(new LineDataset().label("Pan Mean").fill(false))
            .addDataset(new LineDataset().label("Pan Max").fill(false)).and().options().responsive(true).title().display(true).text("Pan/Core overview").and().tooltips().mode(InteractionMode.INDEX)
            .intersect(false).and().hover().mode(InteractionMode.NEAREST).intersect(true).and().scales()
            .add(Axis.X, new CategoryScale().display(true).scaleLabel().display(true).labelString("n° of Genomes").and().position(Position.TOP))
            .add(Axis.Y, new LinearScale().display(true).scaleLabel().display(true).labelString("Ratio").and().ticks().suggestedMin(0).suggestedMax(1).and().position(Position.RIGHT)).and().done();

        List<String> labels = lineConfig.data().getLabels();
        System.out.println(StringUtils.join(labels, "\nX "));

        for (Dataset<?, ?> ds : lineConfig.data().getDatasets()) {
            String[] tableArray = tableScanner.nextLine().split("\t");
            List<Double> line = new ArrayList<>();
            for (String element : tableArray) {
                line.add(Double.parseDouble(element));
            }

            // Casting...
            LineDataset lds = (LineDataset) ds;

            lds.dataAsList(line);
            color = ColorUtils.randomColor(0.3);
            lds.borderColor(color);
            lds.backgroundColor(color);
        }

        ChartJs chart = new ChartJs(lineConfig);
        chart.setJsLoggingEnabled(true);
        // chart.addClickListener((a,b) -> DemoUtils.notification(a, b, lineConfig.data().getDatasets().get(a)));
        return chart;
    }
}
