package nl.wur.ssb;


import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.wur.ssb.Objects.GenericFunctions;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;
import org.vaadin.gridutil.cell.GridCellFilter;

public class SparqlView extends VerticalLayout implements View {

  /*
  Current TODOs/BUGs:

    Can only query one hardcoded genome
    Layout bottom grid is falling of the page on laptop not on ext. screen...
    Grid to be exported to excel or tabular file....
   */

    // Global grid store to have access for saving...
    private Grid<HashMap<String, String>> grid = null;
    private TabSheet tabSheet = new TabSheet();
    private VerticalLayout genomeTab = new VerticalLayout();
    private VerticalLayout sparqlTab = new VerticalLayout();

    public SparqlView() {
        addComponent(tabSheet);

        /*
        SPARQL INTERFACE AREA LEFT
         */

        VerticalLayout sparqlArea = new VerticalLayout();

        TextArea sparqlBox = new TextArea();
        sparqlBox.setSizeFull();

        String PREFIX = "PREFIX gbol:<http://gbol.life/0.1/>\nSELECT ?gene\nWHERE {\n  ?gene a gbol:Gene .\n} LIMIT 10";
        sparqlBox.setValue(PREFIX);
        sparqlBox.setRows(15);

        // Have a component that fires click events
        Button query = new Button("Execute query");
        query.setClickShortcut(ShortcutAction.KeyCode.ENTER, ShortcutAction.ModifierKey.CTRL);
        Button download = new Button("Download results");

        sparqlArea.addComponent(sparqlBox);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        sparqlArea.addComponent(buttonLayout);
        buttonLayout.addComponent(query);
        buttonLayout.addComponent(download);

        /*
        QUERY EXAMPLES
         */

        VerticalLayout queryExamples = new VerticalLayout();
        queryExamples.addComponent(createExampleLabel(sparqlBox));

        /*
        TAB SHEET
         */

        tabSheet.setStyleName("tabSheet");
        tabSheet.addTab(genomeTab, "Genome selection");
        tabSheet.addTab(sparqlTab, "SPARQL interface");

        /*
        SPARQL TAB SHEET
         */

        // Adding sections for Query and Examples to the tabsheet
        sparqlTab.setWidth(100, Unit.PERCENTAGE);
        HorizontalLayout queryLayout = new HorizontalLayout();
        queryLayout.setWidth(100, Unit.PERCENTAGE);
        queryLayout.addComponent(sparqlArea);
        queryLayout.addComponent(queryExamples);
        queryLayout.setExpandRatio(sparqlArea, 70);
        queryLayout.setExpandRatio(queryExamples, 30);
        sparqlTab.addComponent(queryLayout);

        // Horizontal layout for the grid? Such that we can clear the layout for new query
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidth(100, Unit.PERCENTAGE);
        sparqlTab.addComponent(horizontalLayout); // Might go wrong here...


        /*
        GENOME TAB SHEET
         */

        Grid<Genome> genomeGrid = new Grid<>();
        genomeGrid.setWidth("100%");
        genomeGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        genomeGrid.setItems(MyUI.genomes);
        genomeGrid.addColumn(Genome::getScientificName).setCaption("Scientific name").setId("name");
        genomeGrid.addColumn(Genome::getStrain).setCaption("Strain").setId("strain");
        genomeGrid.addColumn(Genome::getSpecies).setCaption("Species").setId("species");
        genomeGrid.addColumn(Genome::getGenus).setCaption("Genus").setId("genus");
        genomeGrid.addColumn(Genome::getFamily).setCaption("Family").setId("family");
        genomeGrid.addColumn(Genome::getOrder).setCaption("Order").setId("order");
        genomeGrid.addColumn(Genome::getClazz).setCaption("Class").setId("class");
        genomeGrid.addColumn(Genome::getPhylum).setCaption("Phylum").setId("phylum");
        genomeGrid.addColumn(Genome::getSuperkingdom).setCaption("Superkingdom").setId("superkingdom");
        GridCellFilter<Genome> filter = new GridCellFilter<>(genomeGrid, Genome.class);
        filter.setTextFilter("name", true, false, "Scientific Name...");
        filter.setTextFilter("strain", true, false, "Strain...");
        filter.setTextFilter("species", true, false, "Species...");
        filter.setTextFilter("genus", true, false, "Genus...");
        filter.setTextFilter("family", true, false, "Family...");
        filter.setTextFilter("order", true, false, "Order...");
        filter.setTextFilter("class", true, false, "Class...");
        filter.setTextFilter("phylum", true, false, "Phylum...");
        filter.setTextFilter("superkingdom", true, false, "Superkingdom...");
        genomeTab.addComponents(genomeGrid);

        // IF SPARQL ENDPOINT IS SET WE DO NOT SHOW THE GENOME SELECTION AS ALL IS IN THE ENDPOINT
        if (MyUI.rdfSimpleCon.server != null) {
            tabSheet.removeComponent(genomeTab);
        }

        // Executing query and generate results function
        query.addClickListener((ClickListener) event -> {
            try {
                grid = Sparql(sparqlBox.getValue(), genomeGrid.getSelectedItems());
                if (grid != null) {
                    grid.setWidth(100, Unit.PERCENTAGE);
                }
                saveQuery(sparqlBox.getValue());
                // Removes component if exists?
                horizontalLayout.removeAllComponents();
                // And ads afterwards with new content?
                if (grid != null) {
                    horizontalLayout.addComponent(grid);
                }

            } catch (QueryParseException e) {
                Notification.show("Query is incorrect", e.getMessage(), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        });

        download.addClickListener((ClickListener) event -> {
            if (grid != null) {
                grid.setWidth(100, Unit.PERCENTAGE);
                Notification.show("Download not working yet :(", Type.WARNING_MESSAGE);
            }
        });
    }

    private Grid<HashMap<String, String>> Sparql(String queryString, Set<Genome> selectedGenomes) {

        List<QuerySolution> results = new ArrayList<>();

        // Endpoint selected
        MyUI.logger.info("Endpoint? : " + MyUI.rdfSimpleCon.server);
        if (MyUI.rdfSimpleCon.server != null) {
            org.apache.jena.query.Query query = QueryFactory.create(queryString);
//
            String server = "http://" + MyUI.rdfSimpleCon.server+ ":" + MyUI.rdfSimpleCon.port + "/" + MyUI.rdfSimpleCon.finalLocation;
            QueryExecution qe = QueryExecutionFactory.sparqlService(server, query);
            qe.setTimeout(7L, TimeUnit.DAYS);
            org.apache.jena.query.ResultSet result = qe.execSelect();
//
            while (result.hasNext()) {
                results.add(result.next());
                if (results.size() > 100001) {
                    MyUI.logger.info("BREAKING THE 100001");
                    break;
                }
            }

            MyUI.logger.info("vars : " + StringUtils.join(result.getResultVars(),"\t"));
            MyUI.logger.info("results : " + results.size());
//             Output query results (list) into a single grid...
            MyUI.logger.info("ResultsSize: " + results.size());
            if (results.size() > 0)
                grid = GenericFunctions.DynamicGridJena(results, result.getResultVars());

//             Important - free up resources used running the query
            qe.close();

//             A maximum of 100.000 elements are shown
            if (results.size() > 100000) {
                MyUI.logger.warn("More than 100.000 elements exceeded");
                Notification.show("Results limited to 100.000 items", Type.WARNING_MESSAGE);
            }
            return grid;
        }

        // Select genome from GRID
        if (selectedGenomes.size() == 0) {
            Notification.show("No genome(s) selected", Type.WARNING_MESSAGE);
            return grid;
        }
        try {
            for (Genome genome : selectedGenomes) {
                // Querying the HDT files
                if (genome.getPath() != null) {
                    MyUI.logger.info(genome.getPath());
                    MyUI.logger.info("Querying: " + genome.getPath().getAbsolutePath());
                    HDT hdt = HDTManager.mapIndexedHDT(genome.getPath().getAbsolutePath(), null);
                    HDTGraph graph = new HDTGraph(hdt);
                    Model model = ModelFactory.createModelForGraph(graph);

                    org.apache.jena.query.Query query = QueryFactory.create(queryString);
                    QueryExecution qe = QueryExecutionFactory.create(query, model);
                    org.apache.jena.query.ResultSet result = qe.execSelect();

                    while (result.hasNext()) {
                        results.add(result.next());
                    }

                    // Output query results (list) into a single grid...
                    grid = GenericFunctions.DynamicGridJena(results, result.getResultVars());

                    // Important - free up resources used running the query
                    qe.close();
                    model.close();
                    graph.close();
                    hdt.close();

                    // A maximum of 100.000 elements are shown
                    if (results.size() > 100000) {
                        MyUI.logger.warn("More than 100.000 elements exceeded");
                        Notification.show("Results limited to 100.000 items", Type.WARNING_MESSAGE);
                        return grid;
                    }
                }
            }
            return grid;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param sparqlBox output TextArea
     * @return examples
     */
    private Panel createExampleLabel(TextArea sparqlBox) {
        Panel examples = new Panel("Examples");
        VerticalLayout panelLayout = new VerticalLayout();
        examples.setContent(panelLayout);
        for (Map.Entry<String, List<String>> query : sparqlQueries().entrySet()) {
            Button insertQuery = new Button("Show", clickEvent -> sparqlBox.setValue(query.getValue().get(0)));
            insertQuery.setStyleName("showQueryButton");
            insertQuery.setHeight(20, Unit.PIXELS);
            insertQuery.setWidth(50, Unit.PIXELS);
            Label queryDesc = new Label(query.getValue().get(1));
            queryDesc.setWidth(100, Unit.PERCENTAGE);
            panelLayout.addComponents(queryDesc, insertQuery);
        }
        return examples;
    }


    /**
     * Add another query:
     * ----------------------------------------------------------------------------------------
     * queries.put("allSamples",new ArrayList<>(Arrays.asList(    <-- "allSamples" change name for query call
     * <p>
     * "PREFIX gbol:<http://gbol.life/0.1/>\n"                    <-- put Query here
     * <p>
     * ,"Select all samples from GBOL database"                   <-- description to be displayed for user (mind/leave the comma at the start)
     * )));
     * ----------------------------------------------------------------------------------------
     */

    // TODO parse it from a text file or internal folder with header #* KEYWORD: TITLE
    private Map<String, List<String>> sparqlQueries() {
        Map<String, List<String>> queries = new HashMap<>();
        // Parse queryExamples folder
        String[] files = {"getDNAObjects.sparql", "getFeatures.sparql", "getSample.sparql", "getSampleXref.sparql", "getGenesandProteinsOnContigs.sparql", "getAllSamples.sparql"};
        Pattern pattern = Pattern.compile("(#.*?\n)");
        try {
            // Collecting overall prefixes
            String prefix = Util.readFile("queries/header.txt");
            for (String file : files) {
                String query = Util.readFile("queryExamples/" + file);
//                System.out.println(query);
                Matcher matcher = pattern.matcher(query);
                String header = file;
                if (matcher.find()) {
                    header = matcher.group(1).replaceFirst("#\\*", "").replaceFirst("\n", "");
                }

                // Removing comment lines
                query = query.replaceAll("#.*?\n", "");

                queries.put(file, new ArrayList<>(Arrays.asList(prefix + "\n" + query, header)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return queries;
    }

    private void saveQuery(String query) {
        String fileName = new SimpleDateFormat("yyyyMMddHHmmss'.txt'").format(new Date());
        // TODO other location to create the folders
        new File(MyUI.savePath + "Queries/").mkdirs();
        try (PrintWriter out = new PrintWriter(MyUI.savePath + "Queries/" + fileName)) {
            out.println(query);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Something upon entering
    }
}
