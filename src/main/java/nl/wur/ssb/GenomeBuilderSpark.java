//package nl.wur.ssb;
//
//import com.vaadin.ui.Grid;
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Iterator;
//import java.util.List;
//import nl.wur.ssb.Objects.Genome;
//import nl.wur.ssb.RDFSimpleCon.ResultLine;
//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.rdfhdt.hdt.hdt.HDT;
//import org.rdfhdt.hdt.hdt.HDTManager;
//
//public class GenomeBuilder {
//
//    /**
//     * Generates the genome table given a directory from MyUI
//     *
//     * @return grid object with genomes and lineages
//     */
//    public static Grid<Genome> makeTable() {
//
//        Grid<Genome> grid = new Grid<>();
//        grid.setWidth("100%");
//
//
//        // TODO make it SPARK aware
//        File[] files = new File(MyUI.genomePath).listFiles((d, name) -> name.endsWith(".dat") && name.startsWith("GCA_"));
//        List<File> filesArray = Arrays.asList(files);
//
//        int count = 0;
//
//        // SPARK SETUP
//
//        // Parallize downloading the GCAs
//        SparkConf sConf = new SparkConf();
//
//        sConf.setMaster("local[" + "1" + "]");
//        sConf.setAppName("SAGER");
//        sConf.set("spark.executor.memory", "5g");
//        sConf.set("", "");
//
//        JavaSparkContext sc = new JavaSparkContext(sConf);
//        JavaRDD<File> rddFiles = sc.parallelize(filesArray);
//        List<Genome> genomes = rddFiles.flatMap(file -> retrieve(file)).collect();
//
//        grid.setItems(genomes);
//        grid.addColumn(Genome::getScientificName).setCaption("Scientific name");
//        grid.addColumn(Genome::getStrain).setCaption("Strain");
//        grid.addColumn(Genome::getSpecies).setCaption("Species");
//        grid.addColumn(Genome::getGenus).setCaption("Genus");
//        grid.addColumn(Genome::getFamily).setCaption("Family");
//        grid.addColumn(Genome::getOrder).setCaption("Order");
//        grid.addColumn(Genome::getClazz).setCaption("Class");
//        grid.addColumn(Genome::getPhylum).setCaption("Phylum");
//        grid.addColumn(Genome::getSuperkingdom).setCaption("Superkingdom");
//
//        return grid;
//    }
//
//    private static <U> Iterator<Genome> retrieve(File file) {
//        try {
//            // Connect the genome file
//            HDT hdt = HDTManager.mapIndexedHDT(file.getAbsolutePath(), null);
//
//            // Execute query and get first result assuming 1 sample per genome
//            for (ResultLine resultLine : MyUI.rdfSimpleCon.runQuery(hdt, "getSample.sparql")) {
//
//                // Creating the genome object and fill it with information
//                Genome genome = new Genome();
//
//                genome.setScientificName(resultLine.getLitString("scientificName"));
//                genome.setDescription("description here");
//                genome.setStrain(resultLine.getLitString("strain"));
//
//                // Creating the taxon info hashmap
//                List<String> taxonList = createTaxonLookup("http://purl.uniprot.org/taxonomy/" + resultLine.getLitInt("taxonid"));
//                //
//                if (taxonList != null) {
//                    // Should not be needed when creating a global grid object
//                    //  taxonLookup.put(String.valueOf(resultLine.getLitInt("taxonid")), taxonList);
//                    genome.setSpecies(taxonList.get(0));
//                    genome.setGenus(taxonList.get(1));
//                    genome.setFamily(taxonList.get(2));
//                    genome.setOrder(taxonList.get(3));
//                    genome.setClazz(taxonList.get(4));
//                    genome.setPhylum(taxonList.get(5));
//                    genome.setSuperkingdom(taxonList.get(6));
//                } else {
//                    System.err.println("Taxon not found: " + "http://purl.uniprot.org/taxonomy/" + resultLine.getLitInt("taxonid"));
//                }
//
//                genome.setPath(file);
//                genome.setID(file.getName());
//                //TODO keep object genomes
//                hdt.close();
//                List<Genome> temp = new ArrayList<>();
//                temp.add(genome);
//                return temp.iterator();
//
//            }
//        } catch (Exception e) {
//            System.out.println("error" + e);
//        }
//        return null;
//    }
//
//    /**
//     * Generates a lineage list of Genus, Family, Order, Class, Phylum, Superkingdom
//     *
//     * @param taxonid the taxonomic identifier of the organism
//     * @return a list of the lineage
//     */
//    private static List<String> createTaxonLookup(String taxonid) {
//        try {
//            Iterable<ResultLine> results = null;
//            if (new File(MyUI.genomePath + "taxonomy.hdt").exists()) {
//                HDT taxonomy = HDTManager.mapIndexedHDT(MyUI.genomePath + "taxonomy.hdt", null);
//                results = MyUI.rdfSimpleCon.runQuery(taxonomy, "getLineage.sparql", taxonid);
//            } else {
//                MyUI.logger.error("Taxonomy HDT file cannot be found at: " +MyUI.genomePath + "taxonomy.hdt");
//                return null;
//            }
//
//            List<String> taxonClasses = new ArrayList<>();
//            taxonClasses.add("");
//            taxonClasses.add("");
//            taxonClasses.add("");
//            taxonClasses.add("");
//            taxonClasses.add("");
//            taxonClasses.add("");
//            taxonClasses.add("");
//
//            for (ResultLine resultLine : results) {
//                String parentRank = resultLine.getIRI("parentRank");
//                String typeName = resultLine.getLitString("parentName");
//
//                if (parentRank.matches(".*Species")) {
//                    taxonClasses.set(0, typeName);
//                } else if (parentRank.matches(".*Genus")) {
//                    taxonClasses.set(1, typeName);
//                } else if (parentRank.matches(".*Family")) {
//                    taxonClasses.set(2, typeName);
//                } else if (parentRank.matches(".*Order")) {
//                    taxonClasses.set(3, typeName);
//                } else if (parentRank.matches(".*Class")) {
//                    taxonClasses.set(4, typeName);
//                } else if (parentRank.matches(".*Phylum")) {
//                    taxonClasses.set(5, typeName);
//                } else if (parentRank.matches(".*Superkingdom")) {
//                    taxonClasses.set(6, typeName);
//                }
//            }
//            return taxonClasses;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//
//}
