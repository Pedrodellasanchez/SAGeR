package nl.wur.ssb;

//import static nl.wur.ssb.MyUI.MAINVIEW;
//import static nl.wur.ssb.MyUI.navigator;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;

public class StartView extends VerticalLayout implements View {

    public StartView() {
        setMargin(true);
        setSizeUndefined();

        //GBOL header
        Label header = new Label("SAGER-P");
        header.addStyleName("startHeader");

        //GBOL intro text
        Label introText = new Label("<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum eros ut malesuada lobortis."
            + " Maecenas ligula ipsum, convallis sit amet molestie id, congue id dolor. Cras quis gravida ex. Nunc vitae "
            + "neque at orci ullamcorper lobortis. Etiam vel erat at neque condimentum feugiat id sed nibh. " + "Fusce nec scelerisque leo. Proin gravida dolor in venenatis accumsan. "
            + "Phasellus interdum massa a metus egestas, a congue nulla lobortis." + "<br><br>" + "Nulla scelerisque tellus quis dapibus imperdiet. Ut euismod in magna consequat varius. "
            + "Fusce metus sapien, venenatis quis purus quis, placerat pulvinar quam. Nulla finibus elementum aliquam. "
            + "Vestibulum metus sapien, molestie id velit ac, molestie fermentum justo. Aliquam porta in est finibus scelerisque. "
            + "Nullam lacus quam, efficitur quis ex in, sagittis imperdiet enim. Mauris at lorem vel eros consequat rutrum." + "<br><br>"
            + "Maecenas interdum nulla aliquet, mollis tortor non, tincidunt mauris. " + "Morbi fermentum, ipsum in tempus dictum, massa dui scelerisque sapien, vel pharetra urna mauris at nisi. "
            + "Sed sit amet ligula eget enim cursus ultricies. " + "Pellentesque sollicitudin ante in diam consectetur, quis convallis urna malesuada. "
            + "Duis fermentum neque sit amet lorem vulputate molestie. Vestibulum in vehicula magna. " + "Duis pellentesque metus massa, ut ullamcorper tellus posuere ut. "
            + "Cras viverra, risus vitae scelerisque tempus, ante odio ultrices neque, et ultrices sapien nibh nec quam. "
            + "Sed malesuada ultricies massa at commodo. Nam aliquam id massa nec suscipit. </p>", ContentMode.HTML);

        introText.setWidth("80%");
        introText.addStyleName("introText");

        Label citing = new Label("Citing SAGeR");
        citing.addStyleName("startHeader");

        Label href = new Label("<p><a href=https://www.biorxiv.org/content/early/2017/09/05/184747 " + "target=_blank>SAGeR a semantic web framework for the interoperability of GBOL data</a></p>",
            ContentMode.HTML);
        href.addStyleName("href");

        Label citation = new Label("<p>Jasper J. Koehorst and others</p>" + "<p>bioRxiv Unknown; doi: https://example.com</p>", ContentMode.HTML);
        citation.setWidth("80%");
        citation.addStyleName("introText");

        addComponents(header, introText, citing, href, citation);

        // Setting the progressbar?
        ProgressBar bar = new ProgressBar();
        bar.setEnabled(true);
        bar.setIndeterminate(true);
        bar.setVisible(true);
        addComponent(bar);



    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Something upon entering...
        // Notification.show("Welcome to the Animal Farm");
    }

}
