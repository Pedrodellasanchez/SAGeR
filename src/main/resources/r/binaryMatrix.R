# Title     : TODO
# Objective : TODO
# Created by: peter
# Created on: 28-11-17

library("reshape2")
library("data.table")

args = commandArgs(trailingOnly = TRUE)
imageDir = args[2]
matrix = NULL
allTmpFiles = file(args[1], "r")
tmpFiles = readLines(allTmpFiles, n = - 1)

for (file in tmpFiles) {
    matrix = rbind(matrix, fread(file, sep = "\t", header = FALSE))
}
matrix = cbind(matrix, 1)
colnames(matrix) = c("V1", "V2", "V3")

# Transform into a matrix
matrix = acast(matrix, V1 ~ V2, value.var = "V3", fill = 0)
matrix = t(matrix)

tempFile = paste0(tempfile(tmpdir = imageDir), ".csv")
write.csv(matrix, file = tempFile)
close(allTmpFiles)
cat(tempFile)

