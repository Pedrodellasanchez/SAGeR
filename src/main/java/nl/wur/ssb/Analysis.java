package nl.wur.ssb;


import com.byteowls.vaadin.chartjs.ChartJs;
import com.vaadin.navigator.View;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.apache.commons.compress.utils.IOUtils;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.vaadin.gridutil.cell.GridCellFilter;

public class Analysis extends VerticalLayout implements View {

    //    private Image treeImage;
    private Link treeImage = new Link();
    private ChartJs chart;
    private Button matrixButton;
    //    private ProgressBar progressBar = new ProgressBar(0.0f);

    public Analysis() {
        //         this.setSizeFull();
        initialize();
    }

    public void initialize() {

        HorizontalLayout buttons = new HorizontalLayout();
        Button tree = new Button("tree");
        Button panCore = new Button("pan/core");
        matrixButton = new Button("matrix");

        // set dummy resource to extend fileDownloader to matrixButton for dynamic download in getMatrix()
        StreamResource.StreamSource source = (StreamResource.StreamSource) () -> null;
        StreamResource dummyResource = new StreamResource(source, "");
        FileDownloader fileDownloader = new FileDownloader(dummyResource);
        fileDownloader.extend(matrixButton);

        Grid<Genome> grid = new Grid<>();
        grid.setWidth("100%");
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.setItems(MyUI.genomes);
        grid.addColumn(Genome::getScientificName).setCaption("Scientific name").setId("name");
        grid.addColumn(Genome::getStrain).setCaption("Strain").setId("strain");
        grid.addColumn(Genome::getSpecies).setCaption("Species").setId("species");
        grid.addColumn(Genome::getGenus).setCaption("Genus").setId("genus");
        grid.addColumn(Genome::getFamily).setCaption("Family").setId("family");
        grid.addColumn(Genome::getOrder).setCaption("Order").setId("order");
        grid.addColumn(Genome::getClazz).setCaption("Class").setId("class");
        grid.addColumn(Genome::getPhylum).setCaption("Phylum").setId("phylum");
        grid.addColumn(Genome::getSuperkingdom).setCaption("Superkingdom").setId("superkingdom");
        GridCellFilter<Genome> filter = new GridCellFilter<>(grid, Genome.class);
        filter.setTextFilter("name", true, false, "Scientific Name...");
        filter.setTextFilter("strain", true, false, "Strain...");
        filter.setTextFilter("species", true, false, "Species...");
        filter.setTextFilter("genus", true, false, "Genus...");
        filter.setTextFilter("family", true, false, "Family...");
        filter.setTextFilter("order", true, false, "Order...");
        filter.setTextFilter("class", true, false, "Class...");
        filter.setTextFilter("phylum", true, false, "Phylum...");
        filter.setTextFilter("superkingdom", true, false, "Superkingdom...");
        buttons.addComponents(tree, panCore, matrixButton);
        addComponents(grid, buttons);
        // "pfam" should be a user input selection menu
        tree.addClickListener(event -> makeTree(grid.getSelectedItems(), "pfam"));
        panCore.addClickListener(event -> makePanCore(grid.getSelectedItems(), "pfam"));
        matrixButton.addClickListener(event -> getMatrix(grid.getSelectedItems(), "pfam", fileDownloader));

        //        addComponent(progressBar);
        //        progressBar.setEnabled(false);
    }

    /**
     * @param selectedItems items selected through the GRID interface
     * @param selection criteria on which the analysis is based upon (ec, pfam etc...)
     */
    private void makePanCore(Set<Genome> selectedItems, String selection) {
        try {
            // copy the file outside the war package in the future...?
            InputStream initialStream = General.getResourceFile("/r/pancore.R");
            File targetFile = File.createTempFile("rscript", ".R");
            java.nio.file.Files.copy(initialStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            IOUtils.closeQuietly(initialStream);
            // End of that...
            String cmd = "Rscript " + targetFile + " " + getXrefs(selectedItems, selection) + " " + MyUI.imagePath;
            ExecCommand exec = new ExecCommand(cmd);
            // Get matrix location
            String matrix = exec.getOutput();
            File table = new File(matrix);

            /**
             * Create plot...
             */

            chart = Plots.pancore(table);
            chart.setWidth(90, Unit.PERCENTAGE);
            chart.setHeight(20, Unit.CM);

            clear();
            addComponent(chart);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clear() {

        if (treeImage != null) {
            ;
        }
        removeComponent(treeImage);
        if (chart != null) {
            removeComponent(chart);
        }
    }

    /**
     * @param selectedItems items selected through the GRID interface
     * @param selection criteria on which the analysis is based upon (ec, pfam etc...)
     */

    private void makeTree(Set<Genome> selectedItems, String selection) {
        // Executing R
        try {
            // copy the file outside the war package in the future...?
            InputStream initialStream = General.getResourceFile("/r/tree.R");
            File targetFile = File.createTempFile("rscript", ".R");
            java.nio.file.Files.copy(initialStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            IOUtils.closeQuietly(initialStream);
            // End of that...
            String cmd = "Rscript " + targetFile + " " + getXrefs(selectedItems, selection) + " " + MyUI.imagePath;
            ExecCommand exec = new ExecCommand(cmd);
            // Get figure location
            String image = exec.getOutput();
            FileResource resource = new FileResource(new File(image));
            System.out.println(resource.getFilename());
            // Removes previous things / plots / images /  generated by user
            clear();

            //adds clickable (actually link with icon, not image) image of tree - links to full image in new tab
            treeImage.setResource(resource);
            treeImage.setTargetName("_blank");
            treeImage.setIcon(resource);
            treeImage.setStyleName("link");

            addComponents(treeImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getMatrix(Set<Genome> selectedItems, String selection, FileDownloader fileDownloader) {
        // Executing R
        try {
            InputStream initialStream = General.getResourceFile("/r/binaryMatrix.R");
            File targetFile = File.createTempFile("rscript", ".R");
            java.nio.file.Files.copy(initialStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            IOUtils.closeQuietly(initialStream);
            // End of that...
            String cmd = "Rscript " + targetFile + " " + getXrefs(selectedItems, selection) + " " + MyUI.imagePath;
            System.out.println(cmd);
            ExecCommand exec = new ExecCommand(cmd);
            // Get file location
            String matrix = exec.getOutput();
            FileResource resource = new FileResource(new File(matrix));
            fileDownloader.setFileDownloadResource(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String getXrefs(Set<Genome> selectedItems, String selection) {
        try {
            File tempFiles = File.createTempFile("genomes_" + selection, ".tmp");
            FileWriter writerOfTempFiles = new FileWriter(tempFiles);

            //            float increase = 100 / selectedItems.size();
            //            MyUI.logger.info("PROGRESS BAR VALUE: " + progressBar.getValue());
            //            progressBar.setValue(0.0f);
            //            progressBar.setEnabled(true);

            for (Genome genome : selectedItems) {
                //                progressBar.setValue(progressBar.getValue() + increase);
                //                MyUI.logger.info("PROGRESS BAR VALUE: " + progressBar.getValue());
                MyUI.logger.debug("Genome path: " + genome.getPath());

                try {
                    HDT hdt = HDTManager.mapIndexedHDT(genome.getPath().getAbsolutePath(), null);
                    // TODO make query depending on users preference...
                    // Iterable<ResultLine> results = MyUI.rdfSimpleCon.runQuery(hdt, "getXrefs.sparql", selection);
                    Iterable<ResultLine> results = SPARQLInterface.query(hdt, "getXrefs.sparql", selection);

                    List<String> accessions = new ArrayList<>();
                    results.forEach((result) -> accessions.add(result.getLitString("accession")));

                    // Write to multiple files each genome one...
                    File temp = File.createTempFile("genome_" + selection, ".tmp");
                    FileWriter writer = new FileWriter(temp);
                    for (String accession : accessions) {
                        writer.write(genome.getScientificName() + "\t" + accession + "\n");
                    }
                    writerOfTempFiles.write(temp.getAbsolutePath() + "\n");
                    MyUI.logger.info(temp.getAbsolutePath());
                    writer.close();
                    // hdt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            writerOfTempFiles.close();
            return tempFiles.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
