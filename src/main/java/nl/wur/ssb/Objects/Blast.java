package nl.wur.ssb.Objects;

import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ebi.uniprot.dataservice.client.Client;
import uk.ac.ebi.uniprot.dataservice.client.ServiceFactory;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.Alignment;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.BlastInput;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.BlastResult;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.UniProtBlastService;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.UniProtHit;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.AlignmentCutoffOption;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.DatabaseOption;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.MatrixOption;

/**
 * This class provides a simple example of submitting blast jobs through the UniProtJAPI.
 */
public class Blast extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(Blast.class);
    private String querySequence;
    private DatabaseOption selectedDatabase;
    private MatrixOption selectedMatrix;
    private AlignmentCutoffOption selectedHits;

    public Blast(String sequence, DatabaseOption selectedDatabase, MatrixOption selectedMatrix, AlignmentCutoffOption selectedHits) {
        this.querySequence = sequence;
        this.selectedDatabase = selectedDatabase;
        this.selectedMatrix = selectedMatrix;
        this.selectedHits = selectedHits;

    }

    public List<BlastHit> runUniProtBlast() {
        List<BlastHit> blastHitList = new ArrayList<>();
        List<Alignment> alignment;
        logger.info("Start UniProt blast");

        // Create UniProt blast service
        ServiceFactory serviceFactoryInstance = Client.getServiceFactoryInstance();
        UniProtBlastService uniProtBlastService = serviceFactoryInstance.getUniProtBlastService();
        uniProtBlastService.start();

        System.err.println("dbname: " + selectedDatabase.getDbName());
        System.err.println("dispname: " + selectedDatabase.getDisplayName());
        // Create a blast input with a Database and sequence

        BlastInput input = new BlastInput.Builder(selectedDatabase, querySequence).withMatrix(selectedMatrix).withMaximumNumberOfAlignments(selectedHits).build();

        CompletableFuture<BlastResult<UniProtHit>> resultFuture = uniProtBlastService.runBlast(input);

        try {

            BlastResult<UniProtHit> blastResult = resultFuture.get();

            logger.info("Number of blast hits: " + blastResult.getNumberOfHits());

            for (UniProtHit hit : blastResult.hits()) {
                alignment = hit.getSummary().getAlignments();
                BlastHit blastHit = new BlastHit();
                blastHit.setHitNumber(hit.getSummary().getHitNumber());
                blastHit.setAccessionNumber(hit.getSummary().getEntryAc());
                blastHit.setDescription(hit.getSummary().getDescription());
                blastHit.setDb(databaseName(hit.getSummary().getDatabase()));
                blastHit.setLength(hit.getSummary().getSequenceLength());
                blastHit.setAlignment(hit.getSummary().getAlignments());
                blastHit.setBitScore(alignment.get(0).getBitScore());
                blastHit.setIdentity(alignment.get(0).getIdentity());
                blastHit.setPositives(alignment.get(0).getPositives());
                blastHit.setExpectation(alignment.get(0).getExpectation());
                blastHit.setQuerySeqStart(alignment.get(0).getStartQuerySeq());
                blastHit.setQuerySeqEnd(alignment.get(0).getStartQuerySeq());
                blastHit.setQuerySeq(alignment.get(0).getQuerySeq());
                blastHit.setMatchSeqStart(alignment.get(0).getStartMatchSeq());
                blastHit.setMatchSeqEnd(alignment.get(0).getEndMatchSeq());
                blastHit.setMatchSeq(alignment.get(0).getMatchSeq());
                blastHit.setPattern(alignment.get(0).getPattern());
                blastHitList.add(blastHit);

            }
            // Catch any potential execution exceptions from the blast job
        } catch (ExecutionException e) {
            logger.error(e.getCause().getMessage());
            Notification.show(null, e.getCause().getMessage(), Notification.Type.ERROR_MESSAGE);
            // Will only be thrown if the job is interrupted
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            Notification.show(null, e.getMessage(), Notification.Type.ERROR_MESSAGE);
        } finally {
            uniProtBlastService.stop();
        }

        logger.info("Finished UniProt blast");
        return blastHitList;
    }

    private String databaseName(String dbShort) {
        String fullDBname = "";
        switch (dbShort) {
            case "TR":
                fullDBname = "TrEMBL";
                break;
            case "SP":
                fullDBname = "Swiss-Prot";
                break;
        }
        return fullDBname;
    }
}