package nl.wur.ssb;


import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import nl.wur.ssb.Objects.Blast;
import nl.wur.ssb.Objects.BlastHit;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.Alignment;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.AlignmentCutoffOption;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.DatabaseOption;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.input.MatrixOption;

//TODO extends CustomComponent!?
public class BlastView extends VerticalLayout implements View {

    private HorizontalLayout horizontalLayout;
    private VerticalLayout leftside;
    private VerticalLayout rightside;
    private Button backButton;
    private NativeSelect<DatabaseOption> blastDatabases;
    private NativeSelect<MatrixOption> blastMatrix;
    private NativeSelect<AlignmentCutoffOption> blastHits;
    private TextArea inputSeqArea;
    private String inputSequence = Page.getCurrent().getUriFragment().replaceAll(".*/", "");
    private DatabaseOption selectedDatabase;
    private MatrixOption selectedMatrix;
    private AlignmentCutoffOption maxNumberOfHits;
    private List<Object> ID;

    //TODO add blast result identifier number, save blast result for ? days and create results retrieval
    public BlastView() {
        horizontalLayout = new HorizontalLayout();
        leftside = new VerticalLayout();
        rightside = new VerticalLayout();
        inputSeqArea = new TextArea();

        blastDatabases = new NativeSelect<>("Select a database");
        blastDatabases.setItems(DatabaseOption.values());
        blastDatabases.setEmptySelectionAllowed(false);
        blastDatabases.addSelectionListener(event -> selectedDatabase = event.getValue());
        if (selectedDatabase == null) {
            blastDatabases.setSelectedItem(DatabaseOption.SWISSPROT);
        }

        blastMatrix = new NativeSelect<>("Select a substitution matrix");
        blastMatrix.setItems(MatrixOption.values());
        blastMatrix.setEmptySelectionAllowed(false);
        blastMatrix.addSelectionListener(event -> selectedMatrix = event.getValue());
        if (selectedMatrix == null) {
            blastMatrix.setSelectedItem(MatrixOption.BLOSUM_62);
        }

        blastHits = new NativeSelect<>("Select number of hits returned");
        blastHits.setItems(AlignmentCutoffOption.values());
        blastHits.setEmptySelectionAllowed(false);
        blastHits.addSelectionListener(event -> maxNumberOfHits = event.getValue());
        if (maxNumberOfHits == null) {
            blastHits.setSelectedItem(AlignmentCutoffOption.FIFTY);
        }

        inputSeqArea.setWidth("300px");
        inputSeqArea.setHeight("300px");
        inputSeqArea.setValue(inputSequence);
        backButton = new Button("Back", click -> MyUI.navigator.navigateTo(MyUI.genomeView));
        backButton.setStyleName("backButton");

        //attempt at progress bar
        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        Button blastButton = new Button("BLAST", click -> {
            try {
                runBlast();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Button eraseButton = new Button("Clear entry", click -> inputSeqArea.clear());
        Button retrieveButton = new Button("Retrieve last Sequence", click -> inputSeqArea.setValue(inputSequence));
        leftside.addComponents(backButton);
        leftside.addComponent(inputSeqArea);
        leftside.addComponent(blastDatabases);
        leftside.addComponent(blastMatrix);
        leftside.addComponent(blastHits);
        rightside.addComponents(blastButton, retrieveButton, eraseButton);
        //        horizontalLayout.addComponents(leftside,rightside);
        horizontalLayout.addComponentsAndExpand(leftside, rightside);
        addComponent(horizontalLayout);

    }

    public void runBlast() {

        VerticalLayout buttons = new VerticalLayout();
        buttons.setMargin(false);
        VerticalLayout alignment = new VerticalLayout();
        alignment.setMargin(false);
        VerticalLayout results = new VerticalLayout();
        results.setMargin(false);
        Grid blastOutput;

        backButton = new Button("Back", click -> Page.getCurrent().reload());
        String inputCheck = checkSequenceInput(inputSeqArea.getValue());
        if (!inputSeqArea.isEmpty() && inputCheck != null) {
            ID = (createUniqueID());
            //            System.out.println(ID);
            showPendingPage(ID);

            Blast blaster = new Blast(inputSeqArea.getValue(), selectedDatabase, selectedMatrix, maxNumberOfHits);
            blastOutput = createBlastOutputGrid(blaster.runUniProtBlast(), alignment);

            removeAllComponents();
            buttons.addComponent(backButton);
            results.addComponents(alignment, blastOutput);
            addComponents(buttons, results);
            //            results.setComponentAlignment(blastOutput, com.vaadin.ui.Alignment.BOTTOM_LEFT);
            setExpandRatio(buttons, 10);
            setExpandRatio(results, 90);
        } else {
            Notification blastFail = new Notification("Input sequence not valid", Notification.Type.ERROR_MESSAGE);
            blastFail.setDelayMsec(1000);
            blastFail.show(Page.getCurrent());
        }
    }

    private void showPendingPage(List ID) {

        removeAllComponents();
        //        System.out.println("SPP");
        addComponent(new Label("<h2>Your job is running... Please wait...</h2><br>Your job identifier: " + ID.get(0) + "<br>Request send on: " + ID.get(1), ContentMode.HTML));
        //        MyUI.getCurrent().getContent();
        //        System.out.println("SPP done");
    }

    private String checkSequenceInput(String sequence) {
        sequence = sequence.toUpperCase();
        String check;
        String dnaRegex = "[ACGT]+";
        String protRegex = "[A-Z&&[^BJOUXZ]]+";
        if (sequence.matches(dnaRegex)) {
            check = "dna";
        } else if (sequence.matches(protRegex)) {
            check = "prot";
        } else {
            check = null;
        }
        return check;
    }

    private Grid createBlastOutputGrid(List<BlastHit> blastOutput, VerticalLayout alignment) {

        Grid<BlastHit> blastGrid = new Grid<>();

        blastGrid.setCaption("Blast Output");
        blastGrid.setWidth("100%");
        blastGrid.setHeight("300px");

        blastGrid.addColumn(BlastHit::getHitNumber).setCaption("Hit").setWidth(50);
        Grid.Column<BlastHit, String> linkColumn = blastGrid
            .addColumn(blastHit -> "<a href=http://www.uniprot.org/uniprot/" + blastHit.getAccessionNumber() + " target=_blank>" + blastHit.getAccessionNumber() + "</a>", new HtmlRenderer())
            .setCaption("Accession").setWidth(100);
        blastGrid.addColumn(BlastHit::getDescription).setCaption("Description").setWidth(1100);
        blastGrid.addColumn(BlastHit::getDb).setCaption("Database").setWidth(100);
        blastGrid.addColumn(BlastHit::getLength).setCaption("Length").setWidth(100);
        blastGrid.addColumn(BlastHit::getBitScore).setCaption("Score (Bits)").setWidth(100);
        blastGrid.addColumn(BlastHit::getIdentity).setCaption("Identities %").setWidth(100);
        blastGrid.addColumn(BlastHit::getPositives).setCaption("Positives").setWidth(100);
        blastGrid.addColumn(BlastHit::getExpectation).setCaption("E-factor").setWidth(100);
        blastGrid.addItemClickListener(itemClick -> {
            alignment.removeAllComponents();
            alignment.addComponent(sequenceOutput(blastOutput.get(itemClick.getItem().getHitNumber() - 1)
                .getAlignment()));//" "+blastOutput.get(itemClick.getItem().getHitNumber()-1).getQuerySeq()," "+blastOutput.get(itemClick.getItem().getHitNumber()-1).getMatchSeq()," "+blastOutput.get(itemClick.getItem().getHitNumber()-1).getPattern()));


        });
        blastGrid.setItems(blastOutput);
        return blastGrid;

    }

    public Label sequenceOutput(List<Alignment> alignment) {
        StringBuilder output = new StringBuilder();
        output.append("<table>");
        int index = 0;
        int multiplier = 0;
        int queryGapCount = 0;
        int matchGapCount = 0;

        //change stringSize to change the length of the output strings
        int stringSize = 100;

        String sub;
        while (index < alignment.get(0).getQuerySeq().length()) {
            if (index != 0 && (index % stringSize) == 0) {
                output.append("<tr><td>Query</td><td>");
                output.append(alignment.get(0).getStartQuerySeq() + (stringSize * multiplier));
                output.append("</td>");
                sub = alignment.get(0).getQuerySeq().substring(index - stringSize, index);
                output.append("<td>");
                output.append(sub);
                output.append("</td>");
                queryGapCount += sub.length() - sub.replace("-", "").length();
                output.append("<td>");
                output.append(alignment.get(0).getStartQuerySeq() + index - 1 - queryGapCount);
                output.append("</td></tr>");
                output.append("<tr><td></td><td></td>");
                output.append("<td>");
                output.append(alignment.get(0).getPattern().substring(index - stringSize, index).replaceAll(" ", "&nbsp"));
                output.append("</td>");
                output.append("<td></td></tr>");
                output.append("<tr><td>Subject</td><td>");
                output.append(alignment.get(0).getStartMatchSeq() + (stringSize * multiplier));
                output.append("</td>");
                sub = alignment.get(0).getMatchSeq().substring(index - stringSize, index);
                output.append("<td>");
                output.append(sub);
                output.append("</td><td>");
                matchGapCount += sub.length() - sub.replace("-", "").length();
                output.append(alignment.get(0).getStartMatchSeq() + index - 1 - matchGapCount);
                output.append("</td></tr><tr><td><br></td></tr>");
                multiplier++;
            }
            index += stringSize;
        }
        int moduloIndex = (alignment.get(0).getQuerySeq().length() % stringSize);
        output.append("<tr><td>Query</td><td>");
        output.append(alignment.get(0).getStartQuerySeq() + (stringSize * multiplier) - queryGapCount);
        output.append("</td>");
        sub = alignment.get(0).getQuerySeq().substring(index - stringSize, index - stringSize + moduloIndex);
        output.append("<td>" + sub + "</td>");
        queryGapCount += sub.length() - sub.replace("-", "").length();
        output.append("<td>");
        output.append(alignment.get(0).getStartQuerySeq() + (stringSize * multiplier) + moduloIndex - 1 - queryGapCount);
        output.append("</td></tr>");
        output.append("<tr><td></td><td></td>");
        output.append("<td>" + alignment.get(0).getPattern().substring(index - stringSize, index - stringSize + moduloIndex).replaceAll(" ", "&nbsp") + "</td>");
        output.append("<td></td></tr>");
        output.append("<tr><td>Subject</td><td>");
        output.append(alignment.get(0).getStartMatchSeq() + (stringSize * multiplier) - matchGapCount);
        output.append("</td>");
        sub = alignment.get(0).getMatchSeq().substring(index - stringSize, index - stringSize + moduloIndex);
        output.append("<td>" + sub + "</td><td>");
        matchGapCount += sub.length() - sub.replace("-", "").length();
        output.append(alignment.get(0).getStartMatchSeq() + (stringSize * multiplier) + moduloIndex - 1 - matchGapCount);
        output.append("</td></tr><tr></tr>");

        output.append("</table>");
        Label match = new Label(output.toString(), ContentMode.HTML);
        match.setStyleName("match");
        return match;
    }

    private List<Object> createUniqueID() {
        List<Object> ID = new ArrayList<>();
        UUID uuid = UUID.randomUUID();
        Date date = new Date();
        ID.add(uuid);
        ID.add(date);
        return ID;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
