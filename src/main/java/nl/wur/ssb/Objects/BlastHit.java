package nl.wur.ssb.Objects;

import java.util.List;
import uk.ac.ebi.uniprot.dataservice.client.alignment.blast.Alignment;

public class BlastHit {

    private int hitNumber;
    private String accessionNumber;
    private String description;
    private String db;
    private long length;
    private List<Alignment> alignment;
    private float bitScore;
    private float identity;
    private float positives;
    private double expectation;
    private int querySeqStart;
    private int querySeqEnd;
    private String querySeq;
    private int matchSeqStart;
    private int matchSeqEnd;
    private String matchSeq;
    private String pattern;


    public int getHitNumber() {
        return hitNumber;
    }

    public void setHitNumber(int hitNumber) {
        this.hitNumber = hitNumber;
    }

    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public List<Alignment> getAlignment() {
        return alignment;
    }

    public void setAlignment(List<Alignment> alignment) {
        this.alignment = alignment;
    }


    public float getBitScore() {
        return bitScore;
    }

    public void setBitScore(float bitScore) {
        this.bitScore = bitScore;
    }

    public float getIdentity() {
        return identity;
    }

    public void setIdentity(float identity) {
        this.identity = identity;
    }

    public float getPositives() {
        return positives;
    }

    public void setPositives(float positives) {
        this.positives = positives;
    }

    public double getExpectation() {
        return expectation;
    }

    public void setExpectation(double expectation) {
        this.expectation = expectation;
    }

    public int getQuerySeqStart() {
        return querySeqStart;
    }

    public void setQuerySeqStart(int querySeqStart) {
        this.querySeqStart = querySeqStart;
    }

    public int getQuerySeqEnd() {
        return querySeqEnd;
    }

    public void setQuerySeqEnd(int querySeqEnc) {
        this.querySeqEnd = querySeqEnc;
    }

    public String getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(String querySeq) {
        this.querySeq = querySeq;
    }

    public int getMatchSeqStart() {
        return matchSeqStart;
    }

    public void setMatchSeqStart(int matchSeqStart) {
        this.matchSeqStart = matchSeqStart;
    }

    public int getMatchSeqEnd() {
        return matchSeqEnd;
    }

    public void setMatchSeqEnd(int matchSeqEnd) {
        this.matchSeqEnd = matchSeqEnd;
    }

    public String getMatchSeq() {
        return matchSeq;
    }

    public void setMatchSeq(String matchSeq) {
        this.matchSeq = matchSeq;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
