package nl.wur.ssb;

import java.io.InputStream;
import nl.wur.ssb.RDFSimpleCon.Util;

public class General {

    public static InputStream getResourceFile(String file) {
        return Util.class.getResourceAsStream("/" + file);

    }
}
