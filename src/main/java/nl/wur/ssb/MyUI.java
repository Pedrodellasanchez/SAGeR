package nl.wur.ssb;

//import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.cdi.CDIUI;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.util.List;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

//TODO DOUBLE INIT of classes, why???

//TODO You could override View.enter(...) in your ViewB, and,
//TODO according to your application state, update your view URI using
//TODO Page.getCurrent().setUriFragment(recentUrl, false);
//TODO https://stackoverflow.com/questions/36412557/avoid-back-button-using-vaadin

/**
 * This UI is the application entry point. A UI may either represent a browser window (or tab) or
 * some part of a html page where a Vaadin application is embedded. <p> The UI is initialized using
 * {@link #init(VaadinRequest)}. This method is intended to be overridden to add component to the
 * user interface and initialize non-component functionality.
 */

@Title("SAGeR")
@Theme("mytheme")
@CDIUI("")

@Push
public class MyUI extends UI {
    // Endpoint when needed
    String endpoint = "http://10.117.10.44:7200/repositories/DigiSal";

    public static final String startView = "startView";
    public static final String genomeView = "genomeView";
    public static final String genomeBrowser = "genomeBrowser";
    public static final String sparqlView = "sparqlView";
    public static final String analysis = "analysisView";
    public static final String aboutView = "aboutView";
    public static final String helpView = "helpView";
    public static final String blastView = "blastView";
    // Logging functionality
    public static Logger logger;
    //    static final String MAINVIEW = "main";

    /**
     * CHANGE PATH TO LOCAL DIRECTORY automatic through initailzation
     **/

    public static String genomePath;
    public static String tmpPath;
    public static String savePath;
    public static String imagePath;

    // Generic object creation

    public static RDFSimpleCon rdfSimpleCon;
    //    public static Grid<Genome> genomeGrid = new Grid<>();
    public static List<Genome> genomes;

    // Laptop settings Jasper
    //  static final String genomePath = "/Users/jasperkoehorst/website/genomes/";
    //  static final String imagePath = "/Users/jasperkoehorst/website/";
    //  static final String savePath = "/Users/jasperkoehorst/website/Save/";
    //  static final String tmpPath = "/Users/jasperkoehorst/website/";
    static Navigator navigator;

    public static Logger Logger(boolean debug) {
        Logger logger = Logger.getLogger(Logger.class);
        Logger.getRootLogger().setLevel(Level.INFO); // changing log level
        Logger.getRootLogger().setAdditivity(false); // avoiding redundancy
        if (debug) {
            Logger.getRootLogger().setLevel(Level.DEBUG); // changing log level
        }
        return logger;
    }

    @Override

    protected void init(VaadinRequest vaadinRequest) {
        // Setting the overall logger
        logger = Logger(true);

        // Setting the theme
        setTheme("mytheme");

        getPage().setTitle("SAGeR");
        VerticalLayout rootLayout = new VerticalLayout();
        rootLayout.setSizeFull();
        //    rootLayout.setSizeUndefined();
        rootLayout.setMargin(false);
        rootLayout.setSpacing(false);

        /**
         * Initializing import parts of the website....
         */

        initializations();

        /**
         * CREATING THE HEADER
         */

        HorizontalLayout menuBar = new HorizontalLayout();

        menuBar.setStyleName("banner");
        rootLayout.addComponent(menuBar);
        rootLayout.setExpandRatio(menuBar, 10);
        // Homepage
        Button startViewButton = new NativeButton("SAGeR-P");
        startViewButton.addClickListener(clickEvent -> navigator.navigateTo("startView"));
        startViewButton.setStyleName("bannerLabel");

        // Genomepage
        Button genomeViewButton = new NativeButton("Genomes");
        genomeViewButton.addClickListener(clickEvent -> navigator.navigateTo("genomeView"));
        genomeViewButton.setStyleName("bannerLabel");

        // Aboutpage
        Button aboutViewButton = new NativeButton("About");
        aboutViewButton.addClickListener(clickEvent -> navigator.navigateTo("aboutView"));
        aboutViewButton.setStyleName("bannerLabel");

        // SPARQL interface
        Button sparqlViewButton = new NativeButton("SPARQL");
        sparqlViewButton.addClickListener(clickEvent -> navigator.navigateTo("sparqlView"));
        sparqlViewButton.setStyleName("bannerLabel");
        // SPARQL interface
        Button analysisViewButton = new NativeButton("Analysis");
        analysisViewButton.addClickListener(clickEvent -> navigator.navigateTo("analysisView"));
        analysisViewButton.setStyleName("bannerLabel");

        // Helppage
        Button helpViewButton = new NativeButton("Help");
        helpViewButton.addClickListener(clickEvent -> navigator.navigateTo("helpView"));
        helpViewButton.setStyleName("bannerLabel");

        // Connecting
        menuBar.addComponent(startViewButton);
        menuBar.addComponent(genomeViewButton);
        menuBar.addComponent(sparqlViewButton);
        menuBar.addComponent(analysisViewButton);
        menuBar.addComponent(aboutViewButton);
        menuBar.addComponent(helpViewButton);

        /**
         * END OF HEADER
         */

        // Creates a section that is allowed to change depending on the page
        Panel contentLayoutPanel = new Panel();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSizeFull();
        //    contentLayout.setSizeUndefined();
        //    contentLayout.setWidth("100%");
        contentLayout.setMargin(false);
        contentLayout.setSpacing(false);
        contentLayout.setStyleName("content");
        //    contentLayoutPanel.setWidth("100%");
        //    contentLayoutPanel.setHeight("100%");
        contentLayoutPanel.setSizeFull();
        //    contentLayoutPanel.setSizeUndefined();
        contentLayoutPanel.setContent(contentLayout);
        rootLayout.addComponent(contentLayoutPanel);

        //    rootLayout.addComponent(contentLayout);
        rootLayout.setExpandRatio(contentLayoutPanel, 90);
        //    rootLayout.setExpandRatio(contentLayout, 90);

        // Create a navigator to control the views
        // TODO navigator (and more):
        // TODO https://www.youtube.com/watch?v=_W_NTFY8mQs
        navigator = new Navigator(UI.getCurrent(), contentLayout);

        // Create and register the views... Using the class method so that it is not initalized upon startup
        navigator.addView(startView, StartView.class);
        navigator.addView(genomeView, GenomeOverview.class);
        navigator.addView(genomeBrowser, GenomeBrowser.class);
        navigator.addView(sparqlView, SparqlView.class);
        navigator.addView(analysis, Analysis.class);
        navigator.addView(aboutView, AboutView.class);
        navigator.addView(helpView, HelpView.class);
        navigator.addView(blastView, BlastView.class);

        // Set to startView when done... if there is nothing in the URL
        String currentPath = Page.getCurrent().getUriFragment();

        if (currentPath != null && currentPath.contains("!")) {
            navigator.navigateTo(currentPath.replaceFirst("!", ""));
        } else {
            navigator.navigateTo("startView");
        }
        setContent(rootLayout);
    }

    /**
     * General inializations of the domain object, genome table, etc...
     */
    private void initializations() {
        /*
          Setting the paths on different development or runtime systems
         */

        if (new File("/home/peter/Sager/Genomes/").isDirectory()) {
            logger.info("Ubuntu settings Peter");
            genomePath = "/home/peter/Sager/Genomes/";
            tmpPath = "/home/peter/Sager/TMP/";
            savePath = "/home/peter/Sager/Save/";
            imagePath = "/home/peter/Sager/Images/";
        } else if (new File("/home/jasperk/SAGER/Genomes/").isDirectory()) {
            logger.info("WUR Server settings");
            genomePath = "/home/jasperk/SAGER/Genomes/";
            tmpPath = "/home/jasperk/SAGER/TMP/";
            savePath = "/home/jasperk/SAGER/Save/";
            imagePath = "/home/jasperk/SAGER/Images/";
        } else if (new File("/Volumes/Data/Genomes/").isDirectory()) {
            logger.info("iMac settings Jasper");
            genomePath = "/Volumes/Data/Genomes/";
            imagePath = "/Volumes/Data/Images/";
            savePath = "/Volumes/Data/Save/";
            tmpPath = "/Volumes/Data/TMP/";
        } else if (new File("/Docker/Genomes/").isDirectory()) {
            logger.info("iMac settings Jasper");
            genomePath = "/Docker/Genomes/";
            imagePath = "/Docker/Images/";
            savePath = "/Docker/Save/";
            tmpPath = "/Docker/TMP/";
        } else if (new File("/Users/jasperkoehorst/GitLab/SAGeR/Data").isDirectory()) {
            logger.info("laptop settings Jasper");
            genomePath = "/Users/jasperkoehorst/GitLab/SAGeR/Data/Genomes/";
            imagePath = "/Users/jasperkoehorst/GitLab/SAGeR/Data/Images/";
            savePath = "/Users/jasperkoehorst/GitLab/SAGeR/Data/Save/";
            tmpPath = "/Users/jasperkoehorst/GitLab/SAGeR/Data/TMP/";
        }

        // Making directories if needed
        new File(genomePath).mkdirs();
        new File(imagePath).mkdirs();
        new File(savePath).mkdirs();
        new File(tmpPath).mkdirs();

        // RDFSimpleCon creator
        try {
            // RDF engine
            logger.info("Initializing RDFSimpleCon / RDF Engine");

            // Changing to endpoint otherwise empty RDFSimpleCon
            if (endpoint != null)
                rdfSimpleCon = new RDFSimpleCon(endpoint);
            else
                rdfSimpleCon = new RDFSimpleCon("");

//            MyUI.logger.info(rdfSimpleCon.server);
            rdfSimpleCon.setAuthen("admin", "jasper85");
            // To see what all goes wrong when using an endpoint
            genomePath = null;

            // Temp location
            logger.info("Setting up java.io.tmpdir to: " + tmpPath);
            System.setProperty("java.io.tmpdir", tmpPath);

            // Genome grid table initialization
            logger.info("Building genome table");
            genomes = GenomeBuilder.makeTable();
        } catch (Exception e) {
            logger.error("Initialization of domain failed... Website cannot start...");
            logger.error(e.getMessage());
        }

        // TODO We should make a config file as once the WAR is packed we cannot modify this...
    }
}
