package nl.wur.ssb;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class HelpView extends VerticalLayout implements View {

    public HelpView() {
        setSizeFull();

        Label label = new Label("Help View Label");
        addComponent(label);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Something upon entering
        // Notification.show("Welcome to the Genome View");
    }

}
