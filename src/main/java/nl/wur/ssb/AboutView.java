package nl.wur.ssb;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.io.File;

public class AboutView extends VerticalLayout implements View {

    private String imagePath;

    public AboutView() {
        setSizeUndefined();
        //    setSpacing(true);
        //    setWidth("2000px");
        //    Panel panel = new Panel();
        //    panel.setWidth("100%");
        //    panel.setHeight("100%");

        //    VerticalLayout content = new VerticalLayout();
        //    content.setHeight("100%");
        //    content.setWidth("80%");
        this.imagePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF/images/";

        //GBOL header
        Label header = new Label("SAGER-P");
        header.addStyleName("startHeader");

        //GBOL intro text
        Label introText = new Label("<p> GBOL is a genome annotation ontology based on semantic web technologies and " + "provides the means to consistently describe automated genome annotations "
            + "typically found in the GenBank, EBML and GFF formats. Additionally, it can describe " + "the linked data provenance of the abstraction process of genetic information from "
            + "genome sequences. </p>" +

            "<p>An overview of the structure of the GBOL ontology is shown on Figure “GBOL structure”. " + "The essential design principle of GBOL is that sequences have features, which in turn have "
            + "genomic locations on the sequence. These relationships are associated to provenance that "
            + "captures both the statistical basis of each individual annotation (element-wise provenance) " + "and the programs and parameters used for the complete set (dataset-wise provenance). "
            + "All annotations for a given sequence can be packed into a single entity, a document.</p>", ContentMode.HTML);

        introText.setWidth("100%");
        //GBOL overview image
        FileResource resource = new FileResource(new File(imagePath + "overview.svg"));
        Image overview = new Image(null, resource);
        overview.setWidth("1000px");
        overview.setHeight("1000px");

        Label picDescript = new Label(
            "<p><strong>Figure “GBOL structure”: Network based view generated using RDF2Graph of " + "the core of the GBOL ontology. Nodes represent types. Blue edges represent subClassOf "
                + "relationships whereas grey edges represent unique type links. A unique type link is defined "
                + "as a unique tuple: type of subject, predicate, (data)type of object. Arrow heads indicate the "
                + "forward multiplicity of the unique type links: 0..1 and 1..1 multiplicities are indicated " + "by diamonds; 0..N and 1..N multiplicities are indicated by circles.</strong></p>",
            ContentMode.HTML);

        introText.addStyleName("introText");

        Label citing = new Label("Citing SAGeR");
        citing.addStyleName("startHeader");

        Label href = new Label("<p><a href=https://www.biorxiv.org/content/early/2017/09/05/184747 " + "target=_blank>SAGeR a semantic web framework for the interoperability of GBOL data</a></p>",
            ContentMode.HTML);
        href.addStyleName("href");

        Label citation = new Label("<p>Jasper J. Koehorst and others</p>" + "<p>bioRxiv Unknown; doi: https://example.com</p>", ContentMode.HTML);
        citation.setWidth("80%");
        citation.addStyleName("introText");

        //    content.addComponents(header, introText, overview, picDescript, citing, href, citation);
        //    content.setSizeUndefined();
        //    content.setMargin(true);
        addComponents(header, introText, overview, picDescript, citing, href, citation);
        //    content.setSizeUndefined();
        //    content.setMargin(true);
        //    panel.setContent(content);
        //    addComponent(content);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Something upon entering
    }
}
