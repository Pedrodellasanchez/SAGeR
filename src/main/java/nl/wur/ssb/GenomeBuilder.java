package nl.wur.ssb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import nl.wur.ssb.Objects.Genome;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;

public class GenomeBuilder {

    /**
     * Generates the genome table given a directory from MyUI
     *
     * @return grid object with genomes and lineages
     */
    //  public static Grid<Genome> makeTable() {
    public static List<Genome> makeTable() {

        List<Genome> genomes = new ArrayList<>();
        boolean changed = false;

        /*
        CHECK IF GENOME SERIALIZATION FILE EXISTS
         */

        ObjectInputStream objectinputstream = null;
        FileInputStream streamIn = null;
        try {
            streamIn = new FileInputStream(MyUI.genomePath + "/genomes.list");
            objectinputstream = new ObjectInputStream(streamIn);
            List<Genome> tmpGenomes = (List<Genome>) objectinputstream.readObject();
            for (Genome genome : tmpGenomes) {
                if (genome.getPath().exists()) {
                    genomes.add(genome);
                }
            }

            MyUI.logger.info("GENOMES FOUND LOADED CLASS OBJECT FROM DIR");
        } catch (FileNotFoundException e) {
            MyUI.logger.info("Genome file does not yet exist");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        /*
        CREATING GENOME OBJECT WHEN NOT IN LOADED OBJECT
         */

        // TODO make it SPARK aware

        if (MyUI.genomePath != null) {
            List<String> paths = new ArrayList<>();

            for (Genome genome : genomes) {
                paths.add(genome.getPath().getAbsolutePath());
            }

            MyUI.logger.info("GenomePath: " + MyUI.genomePath);

            File[] files = new File(MyUI.genomePath).listFiles((d, name) -> name.endsWith(".dat") && name.startsWith("GCA_"));

            MyUI.logger.info("Number of genomes: " + files.length);
            int count = 0;
            for (File file : files) {
                if (paths.contains(file.getAbsolutePath())) {
                    MyUI.logger.info("Skipping as already in genome object");
                } else {
                    changed = true;
                    count = count + 1;
                    if (count % 100 == 0) {
                        MyUI.logger.info("Parsed " + count + " genomes");
                    }

                    try {
                        // Connect the genome file
                        HDT hdt = HDTManager.mapIndexedHDT(file.getAbsolutePath(), null);

                        // TODO Execute query and get first result assuming 1 sample per genome but should maybe not matter... Testing needed
                        MyUI.logger.info("Testing RDFSimpleCon: " + MyUI.rdfSimpleCon + " on " + file.getAbsolutePath() + " path " + file.getAbsolutePath());
                        for (ResultLine resultLine : SPARQLInterface.query(hdt, "getSample.sparql")) {
                            Genome genome = createGenomeObject(resultLine);
                            genome.setPath(file);
                            genome.setID(file.getName());
                            //TODO keep object genomes
                            genomes.add(genome);
                        }
                        hdt.close();
                    } catch (Exception e) {
                        MyUI.logger.error("error" + e);
                    }
                }
            }
        } else {
            MyUI.logger.info("QUERYING ENDPOINT");
            try {
                for (ResultLine resultLine : SPARQLInterface.query("getSample.sparql")) {
                    MyUI.logger.info("ENDPOINT RESULTS...");
                    Genome genome = createGenomeObject(resultLine);
                    // PATH NOT SET SHOULD THROW ERROR NEED TO SET IRI THEN...
                    genome.setID(genome.getIRI());
                    //TODO keep object genomes
                    genomes.add(genome);
                }
            } catch (Exception e) {
                MyUI.logger.error(e.getMessage());
            }
        }
        /*
        STREAM GENOME DATA TO OBJECT
         */

        if (changed) {
            MyUI.logger.info("Finished loading genomes... Saving to file to never do this again...");
            FileOutputStream fout = null;
            try {
                MyUI.logger.info("Writing genome Object to: " + MyUI.genomePath + "/genomes.list");
                fout = new FileOutputStream(MyUI.genomePath + "/genomes.list");
                ObjectOutputStream oos = new ObjectOutputStream(fout);
                oos.writeObject(genomes);
            } catch (FileNotFoundException e) {
                MyUI.logger.error("FileNotFoundException:\n");
                e.printStackTrace();
            } catch (IOException e) {
                MyUI.logger.error("IOException:\n");
                e.printStackTrace();
            }
        }
        return genomes;
    }

    /**
     *
     * @param resultLine
     * @return
     * @throws Exception
     */
    private static Genome createGenomeObject(ResultLine resultLine) throws Exception {
        MyUI.logger.info("Starting genome object creation");
        // Creating the genome object and fill it with information
        Genome genome = new Genome();

        MyUI.logger.info("Sample");
        genome.setIRI(resultLine.getIRI("sample"));

        MyUI.logger.info("Scientific name");
        if (resultLine.getLitString("scientificName") != null) {
            genome.setScientificName(resultLine.getLitString("scientificName"));
        }

        genome.setDescription("description here");

        if (resultLine.getLitString("strain") != null) {
            genome.setStrain(resultLine.getLitString("strain"));
        }

        // Creating the taxon info hashmap
        List<String> taxonList = null;

        try {
            taxonList = createTaxonLookup("http://purl.uniprot.org/taxonomy/" + resultLine.getLitInt("taxonid"));
            //
            if (taxonList != null) {
                // Should not be needed when creating a global grid object
                //  taxonLookup.put(String.valueOf(resultLine.getLitInt("taxonid")), taxonList);
                genome.setSpecies(taxonList.get(0));
                genome.setGenus(taxonList.get(1));
                genome.setFamily(taxonList.get(2));
                genome.setOrder(taxonList.get(3));
                genome.setClazz(taxonList.get(4));
                genome.setPhylum(taxonList.get(5));
                genome.setSuperkingdom(taxonList.get(6));
            } else {
                MyUI.logger.error("Taxon not found: " + "http://purl.uniprot.org/taxonomy/" + resultLine.getLitInt("taxonid"));
            }
        } catch (Exception e) {
            MyUI.logger.info("No taxon id present... int==null issue");
        }

        return genome;
    }

    /**
     * Generates a lineage list of Genus, Family, Order, Class, Phylum, Superkingdom
     *
     * @param taxonid the taxonomic identifier of the organism
     * @return a list of the lineage
     */
    private static List<String> createTaxonLookup(String taxonid) {
        try {
            Iterable<ResultLine> results = null;
            if (new File(MyUI.genomePath + "taxonomy.hdt").exists()) {
                HDT taxonomy = HDTManager.mapIndexedHDT(MyUI.genomePath + "taxonomy.hdt", null);
                results = SPARQLInterface.query(taxonomy, "getLineage.sparql", taxonid);
            } else {
                MyUI.logger.error("Taxonomy HDT file cannot be found at: " + MyUI.genomePath + "taxonomy.hdt");
                return null;
            }

            List<String> taxonClasses = new ArrayList<>();
            taxonClasses.add("");
            taxonClasses.add("");
            taxonClasses.add("");
            taxonClasses.add("");
            taxonClasses.add("");
            taxonClasses.add("");
            taxonClasses.add("");

            for (ResultLine resultLine : results) {
                String parentRank = resultLine.getIRI("parentRank");
                String typeName = resultLine.getLitString("parentName");

                if (parentRank.matches(".*Species")) {
                    taxonClasses.set(0, typeName);
                } else if (parentRank.matches(".*Genus")) {
                    taxonClasses.set(1, typeName);
                } else if (parentRank.matches(".*Family")) {
                    taxonClasses.set(2, typeName);
                } else if (parentRank.matches(".*Order")) {
                    taxonClasses.set(3, typeName);
                } else if (parentRank.matches(".*Class")) {
                    taxonClasses.set(4, typeName);
                } else if (parentRank.matches(".*Phylum")) {
                    taxonClasses.set(5, typeName);
                } else if (parentRank.matches(".*Superkingdom")) {
                    taxonClasses.set(6, typeName);
                }
            }
            return taxonClasses;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
